# Portable Python Builder for Windows
https://chrisapproved.com/blog/portable-python-for-windows.html
## Description
Script to automate downloading the latest version of Python (official embedded builds) into a directory on your Windows system and optionally installing `pip` for a small portable environment that can be run without administrative privileges.

There is experimental support for installing any version greater than or equal to 3.5.0, however it isn't as robust as I would like.

Tested on Windows 10 with PowerShell version 5.1.19041.1320.

## Installation
Download the script to your local machine and execute with PowerShell. Default install with no parameters will download the latest 64-bit build and extract into a `portablepython` directory where the script is executed from.

## Usage
`PortablePythonBuilder.ps1 [-version <version>] [-arch <architecture>] [-InstallPath <path>] [-getpip]`

#### Examples:
Download the latest version of Python 64-bit:
`.\PortablePythonBuilder.ps1`

Download Python 3.8.1 32-bit and install `pip`:
`.\PortablePythonBuilder.ps1 -version 3.8.1 -arch win32 -getpip`
