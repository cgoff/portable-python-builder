# Copyright (c) 2022 Chris Goff <cgoff@fastmail.fm>
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Ref https://chrisapproved.com/blog/portable-python-for-windows.html

# Version 0.1

param(
    [Parameter()]
    [String]$version,
    [String]$arch = "amd64", # win32 or amd64 are the only acceptable parameters, default to 64-bit
    [String]$InstallPath = "portablepython", # default destination path for portable python
    [Switch]$GetPip
)

Function Get-Latest-Python-Version {
    $HttpReq = Invoke-WebRequest "https://www.python.org/downloads/windows/"
    $extracted = $HttpReq.Links.innerText -like "Latest Python 3 Release*"
    $extracted -replace "Latest Python 3 Release - Python "
}

If ($PSBoundParameters.ContainsKey("version")) {
    Write-Host " WARNING: Experimental " -ForegroundColor Black -BackgroundColor Yellow
    If (([Version] $version) -le ([Version] "3.5.0")) {
        Write-Host " Error: Only versions 3.5.0 and greater have embedded distributions available. Reference https://www.python.org/ftp/python for available versions. " -ForegroundColor Black -BackgroundColor Yellow
        Exit
    }
}
Else {
    $version = Get-Latest-Python-Version
    Write-Host " Downloading Python ($version, $arch) ... " -ForegroundColor Black -BackgroundColor White
}

$PythonDistrib = "python-$version-embed-$arch.zip"

# Download Python
Invoke-WebRequest "https://www.python.org/ftp/python/$version/$PythonDistrib" -ErrorAction Stop -OutFile $PythonDistrib

# Extract it
Write-Host " Extracting to $InstallPath ... " -ForegroundColor Black -BackgroundColor White
Expand-Archive -Path "$PythonDistrib" -DestinationPath $InstallPath

# Edit the _pth file
Write-Host " Modifying PTH file ... " -ForegroundColor Black -BackgroundColor White
$pthFile = Get-Childitem -Path "$InstallPath\*._pth"
$pthFile = $pthFile.Name

# Note this is for backwards compatibility with Windows 10 default PowerShell version 5. Newer version(s) of PowerShell default to
# UTF8NoBom encoding, which is what we are looking for.
Write-Host " Setting proper file encoding on PTH file ... " -ForegroundColor Black -BackgroundColor White
(Get-Content -Path "$InstallPath\$pthFile") -replace '#import site', 'import site' | Out-File -Encoding ASCII $InstallPath\$pthFile 

# set the path to support running Python and PIP
Write-Host " Setting system paths ... " -ForegroundColor Black -BackgroundColor White
If ($InstallPath -eq "portablepython") {
    $CurrentDirectory = Get-Location
    $CurrentDirectory = $CurrentDirectory.Path
    $InstallPath = "$CurrentDirectory\portablepython"
    $Env:PATH += ";$InstallPath;$InstallPath\scripts"
}
Else {
    $Env:PATH += ";$InstallPath;$InstallPath\scripts"
}

# Get PIP installed (if switch exists)
If ($PSBoundParameters.ContainsKey("GetPip")) {
    If (([Version] $version) -ge ([Version] "3.7.0")) {
        # Download pip
        Write-Host " Downloading pip ... " -ForegroundColor Black -BackgroundColor White
        Invoke-WebRequest "https://bootstrap.pypa.io/get-pip.py" -ErrorAction Stop -OutFile $InstallPath\get-pip.py
        # Run installer
        Write-Host " Running pip installer ... " -ForegroundColor Black -BackgroundColor White
        & $InstallPath\python.exe $InstallPath\get-pip.py
    }
    Else {
        Write-Host " Error: The minimum supported version of Python for get-pip.py is 3.7.0. " -ForegroundColor Black -BackgroundColor Yellow
    }
}

# Cleanup
Write-Host " Cleaning up from install ... " -ForegroundColor Black -BackgroundColor White
Remove-Item $InstallPath\get-pip.py
Remove-Item \*.zip

# Done
Write-Host " Done! Run $InstallPath\python.exe to start. " -ForegroundColor Black -BackgroundColor Green
